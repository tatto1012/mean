/* ====== application/scripts/app.js ===-_-=== */
var app = angular.module('MeanApplication',[
    'picardy.fontawesome'
], function(){

});

app.value('_', '_');
app.value('$', '$');
app.value('moment', 'moment');
/* ====== application/scripts/run.js ===-_-=== */
app.run([
    '_regExp',
    '_feedbackMessageObject',
    '_confirmationMessage',
    '$rootScope',
    '$timeout',
function(_regExp, _feedbackMessageObject, _confirmationMessage, $rootScope, $timeout) {
    _.extend($rootScope, _regExp);
    _.extend($rootScope, {'feedbackMessage':_feedbackMessageObject});
    _.extend($rootScope, {'confirmationMessage':_confirmationMessage});

    $rootScope.showFeedbackMessage = function(show, responseData){
        $rootScope.feedbackMessage.animation = show;
        $rootScope.feedbackMessage.message = responseData.data.message;
        $rootScope.feedbackMessage.type = (responseData.status === 200)? 'success' : 'danger';

        $timeout(function(){
            $rootScope.feedbackMessage.animation = false;
        }, $rootScope.feedbackMessage.duration);
    };

    $rootScope.showConfirmMessage = function(message, itemIdentifier){
        $rootScope.confirmationMessage.animation = true;
        $rootScope.confirmationMessage.message = message;
        $rootScope.confirmationMessage.type = 'warning';
        $rootScope.confirmationMessage.itemIdentifier = itemIdentifier;
    };

}]);
/* ====== application/scripts/config.js ===-_-=== */
app.config(function(_config, _develompentVars, _productionVars, $provide){
    switch(_config.appEnvironment){
        case 'Production':
            $provide.constant('_config', _.extend(_config, _productionVars) );
            break;
        case 'Development':
            $provide.constant('_config', _.extend(_config, _develompentVars) );
            break;
    }
});
/* ====== application/scripts/constants.js ===-_-=== */
app.constant('_config', {
    'appReference': 'MEAN',
    'appEnvironment': 'Development'
});

app.constant('_develompentVars', {
    'baseUrl': 'http://localhost:3000',
    'APIroute': 'http://localhost:3000/',
    'requestTimeout': 3000
});

app.constant('_productionVars', {
    'baseUrl': 'http://parking-manager',
    'APIroute': 'http://localhost:3000/',
    'requestTimeout': 60000
});

app.constant('_apiEndpointsList', {
    'getAllParkingSlot': 'api/get-all-parking-slot',
    'getParkingSlotById': 'api/get-parking-slot-by-id',
    'addParkingSlot': 'api/add-parking-slot',
    'editParkingSlot': 'api/edit-parking-slot',
    'removeParkingSlot': 'api/remove-parking-slot'
});

app.constant('_regExp', {
    'onlyNumbersPattern': "^[0-9]+$",
    'onlyAlphabethPattern': "^[a-z _-]+$"
});

app.constant('_feedbackMessageObject', {
    'message':'message not set yet!',
    'animation': false,
    'type': 'warning',
    'duration': 3000
});

app.constant('_confirmationMessage', {
    'message':'message of confirmation not set yet!',
    'animation': false,
    'type': 'warning',
    'duration': 3000,
    'itemIdentifier': false
});
/* ====== application/scripts/controllers/addParkingSlot_controller.js ===-_-=== */
app.controller('addParkingSlotCtrl', [
    '$scope',
    '$rootScope',
    'APIRequestService',
function($scope, $rootScope, APIRequestService) {

    $scope.parkingSlotCode = '';
    $scope.campaign = 'Prodigious';

    $scope.addParkingSlotAction = function(formData){
        if(formData.$valid){
            var requestData = {
                'parkingSlotCode': $scope.parkingSlotCode,
                'campaign': $scope.campaign
            };

            APIRequestService.addParkingSlot(requestData, function(error, response){
                $rootScope.showFeedbackMessage(true, response);
            });
        } else {
            $rootScope.showFeedbackMessage(true, {
                'status': 400,
                'data' : {
                    'message' : "You have and error in your submission"
                }
            });
        }
    };

}]);
/* ====== application/scripts/controllers/editParkingSlot_controller.js ===-_-=== */
app.controller('editParkingSlotCtrl', [
    '$scope',
    '$rootScope',
    'APIRequestService',
function($scope, $rootScope, APIRequestService) {

    $scope.editParkingSlotAction = function(formData){
        if(formData.$valid){
            var requestData = {
                'parkingSlotCode': $scope.parkingSlotCode,
                'campaign': $scope.campaign,
                'slotStatus': $scope.slockStatus,
                'slotIdentifier': $scope.slotIdentifier
            };

            APIRequestService.editParkingSlot(requestData, function(error, response){
                $rootScope.showFeedbackMessage(true, response);
            });
        } else {
            $rootScope.showFeedbackMessage(true, {
                'status': 400,
                'data' : {
                    'message' : "You have and error in your submission"
                }
            });
        }
    };

}]);
/* ====== application/scripts/controllers/parkingSlotsList_controller.js ===-_-=== */
app.controller('parkingSlotsListCtrl', [
    '$scope',
    '$rootScope',
function($scope, $rootScope){
    $scope.verifyRemoveAction = function(slotIdentifier, slotCode){
        $rootScope.showConfirmMessage("Are you sure of remove the <b>" + slotCode + "</b> slot?", slotIdentifier);
    };
}]);
/* ====== application/scripts/directives/confirmationMessage_directive.js ===-_-=== */
app.directive( 'confirmationMessage', function(_config, appHelper){
    return {
        restrict: 'E',
        templateUrl: appHelper.templatePath('confirmation-message'),
        controller: [
            '$scope',
            '$rootScope',
            'APIRequestService',
        function($scope, $rootScope, APIRequestService){
            $scope.cancelAction = function(){
                $rootScope.confirmationMessage.animation = false;
            };

            $scope.confirmAction = function(){
                $rootScope.confirmationMessage.animation = false;
                var requestData = {
                    'slotIdentifier': $rootScope.confirmationMessage.itemIdentifier
                };

                APIRequestService.removeParkingSlot(requestData, function(error, response){
                    $rootScope.showFeedbackMessage(true, response);
                });
            };
        }]
    };
});
/* ====== application/scripts/directives/errorFieldNotification_directive.js ===-_-=== */
app.directive( 'errorFieldNotification', function(_config, appHelper){
    return {
        restrict: 'E',
        templateUrl: appHelper.templatePath('error-field-notification'),
        scope:{
            fieldToValidate: '=fieldToValidate',
        }
    };

});
/* ====== application/scripts/directives/feedbackMessage_directive.js ===-_-=== */
app.directive( 'feedbackMessage', function(_config, appHelper){
    return {
        restrict: 'E',
        templateUrl: appHelper.templatePath('feedback-message')
    };
});
/* ====== application/scripts/directives/mainMenu_directive.js ===-_-=== */
app.directive( 'mainMenu', function(_config, appHelper){
    return {
        restrict: 'E',
        templateUrl: appHelper.templatePath('main-menu'),
        link: function(scope, element, attrs){
            scope.menuItem = {
                listSlots : false,
                addParkingSlot : false
            };

            var currentPage = location.href.replace(_config.baseUrl, '');

            switch(currentPage){
                case '/':
                        scope.menuItem.listSlots = true;
                    break;
                case '/add-parking-slot':
                        scope.menuItem.addParkingSlot = true;
                    break;
            }
        }
    };
});
/* ====== application/scripts/directives/statusIcon_directive.js ===-_-=== */
app.directive( 'statusIcon', function(appHelper){
    return {
        restrict: 'E',
        templateUrl: appHelper.templatePath('status-icon'),
        scope:{
            status: '=status',
        },
        link : function (scope, element, attrs){

            switch(attrs.status){
                case 'available':
                        scope.iconReference = 'circle-o';
                        scope.iconStyle = 'available';
                    break;
                case 'assigned':
                        scope.iconReference = 'check-circle-o';
                        scope.iconStyle = 'assigned';
                    break;
                case 'disabled':
                        scope.iconReference = 'circle';
                        scope.iconStyle = 'disabled';
                    break;
                default:
                        scope.iconReference = 'ellipsis-h';
                        scope.iconStyle = '';
                    break;
            }
        }
    };
});
/* ====== application/scripts/filters/htmlUnSafe_filter.js ===-_-=== */
app.filter('unsafe', function($sce) {
    return function(val) {
        return $sce.trustAsHtml(val);
    };
});
/* ====== application/scripts/services/APIrequest_service.js ===-_-=== */
app.factory('APIRequestService', [
    '_config',
    '_apiEndpointsList',
    '$http',
function (_config, _apiEndpointsList, $http) {
    var APIRequestService = this;

    APIRequestService.madeRequestAction = function(method, endpointReference, requestData, addTokenToRequest, callback){
        //define the http options
        var httpOptions = {
            method : method,
            url    : _config.APIroute + endpointReference,
            data   : requestData,
            cache  : false,
            timeout: _config.requestTimeout
        };

        $http(httpOptions).then(function successCallback(response) {
            callback(null, response);
        }, function errorCallback(response) {
            APIRequestService.handledRequestError(response, callback);
        });
    };

    APIRequestService.handledRequestError = function(errorResponse, callback){
        var errorMessage;

        switch(errorResponse.statusCode){
            case 400:
                    errorMessage = "Bad Request";
                break;
            case 401:
                    errorMessage = "Unauthorized";
                break;
            case 403:
                    errorMessage = "Forbidden";
                break;
            case 404:
                    errorMessage = "Not Found";
                break;
            case 408:
                    errorMessage = "Request Timeout";
                break;
            case 500:
                    errorMessage = "Internal Server Error";
                break;
            case 503:
                    errorMessage = "Service Unavailable";
                break;
            default:
                    errorMessage = 'Without Handled Error with code status ' + errorResponse.statusCode;
                break;
        }

        callback(errorMessage);
    };

    APIRequestService.addParkingSlot = function (requestData, callback){
        APIRequestService.madeRequestAction('POST',_apiEndpointsList.addParkingSlot, requestData,  false, callback);
    };

    APIRequestService.editParkingSlot = function (requestData, callback){
        APIRequestService.madeRequestAction('POST',_apiEndpointsList.editParkingSlot, requestData,  false, callback);
    };

    APIRequestService.removeParkingSlot = function (requestData, callback){
        APIRequestService.madeRequestAction('POST',_apiEndpointsList.removeParkingSlot, requestData,  false, callback);
    };

    return APIRequestService;
}]);
/* ====== application/scripts/services/appHelper.js ===-_-=== */
app.constant('appHelper', {

  templatesDir: '/templates',
  applicationDir: '/application/views',

  templatePath: function(view_name){
    return this.templatesDir + '/' + view_name + '.html';
  },

  applicationView: function(view_name){
    return this.applicationDir + '/' + view_name + '.html';
  }

});