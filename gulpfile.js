var _meanConfig = require('./config/mean.config');

var gulp = require('gulp');
    gutil = require('gulp-util'),
    concat = require('gulp-concat'),
    sass = require('gulp-sass'),
    rename = require('gulp-rename'),
    clean = require('gulp-clean'),
    tap = require('gulp-tap'),
    minifyCss = require('gulp-minify-css'),
    open = require("open"),
    livereload = require('gulp-livereload');

function getFileLoc(file){
  return require('path').relative( './', file.path);
}

gulp.task('default', ['clean', 'sass:app', 'sass:vendor', 'js:app', 'js:vendor', 'open-browser-mean', 'watch']);

gulp.task('sass:app', function(done) {
  gulp.src(_meanConfig.gulpTask.sassPaths.appStyles)
    .pipe(sass())
    .on('error', sass.logError)
    .pipe(rename(_meanConfig.gulpTask.targetNames.appCss))
    .pipe(gulp.dest(_meanConfig.gulpTask.buildDirectories.css))
    // .pipe(minifyCss({ keepSpecialComments: 0 }))
    // .pipe(rename({ extname: '.min.css' }))
    // .pipe(gulp.dest(_meanConfig.gulpTask.buildDirectories.css))
    .pipe(livereload())
    .on('end', done);
});

gulp.task('sass:vendor', function(done) {
  gulp.src(_meanConfig.gulpTask.sassPaths.vendor)
    .pipe(sass())
    .on('error', sass.logError)
    .pipe(rename(_meanConfig.gulpTask.targetNames.vendorCss))
    .pipe(gulp.dest(_meanConfig.gulpTask.buildDirectories.css))
    // .pipe(minifyCss({ keepSpecialComments: 0 }))
    // .pipe(rename({ extname: '.min.css' }))
    // .pipe(gulp.dest(_meanConfig.gulpTask.buildDirectories.css))
    .pipe(livereload())
    .on('end', done);
});

gulp.task('js:app', function(){
  return gulp.src( _meanConfig.gulpTask.appScripts )
  .pipe(tap(function(file){
    var fileLoc = getFileLoc(file);
    file.contents = Buffer.concat([
      new Buffer('/* ====== ' + fileLoc  + ' ===-_-=== */\n'),
      file.contents
    ]);
  }))
  .pipe(concat( _meanConfig.gulpTask.targetNames.appJs ))
  .pipe(gulp.dest( _meanConfig.gulpTask.buildDirectories.js ))
  .pipe(livereload());
});

gulp.task('js:vendor', function(){
  return gulp.src( _meanConfig.gulpTask.vendorScripts )
  .pipe(tap(function(file){
    var fileLoc = getFileLoc(file);
    file.contents = Buffer.concat([
      new Buffer('/* ====== ' + fileLoc  + ' ===-_-=== */\n'),
      file.contents
    ]);
  }))
  .pipe(concat( _meanConfig.gulpTask.targetNames.vendorJs ))
  .pipe(gulp.dest( _meanConfig.gulpTask.buildDirectories.js ))
  .pipe(livereload());
});

gulp.task('refreshPageTrigger', function(){
  return gulp.src('/')
  .pipe(livereload());
});

gulp.task('open-browser-mean', function () {
  return open(_meanConfig.gulpTask.openBrowser.url, _meanConfig.gulpTask.openBrowser.browser);
});

gulp.task('clean', function(){
  return gulp.src( _meanConfig.gulpTask.cleanFilesList, { read:false })
  .pipe(clean());
});

gulp.task('watch', function(){
  livereload.listen();
  gulp.watch(_meanConfig.gulpTask.appStyles, ['sass:app']);
  gulp.watch(_meanConfig.gulpTask.vendorStyles, ['sass:vendor']);
  gulp.watch(_meanConfig.gulpTask.appScripts, ['js:app']);
  gulp.watch(_meanConfig.gulpTask.vendorScripts, ['js:vendor']);
  gulp.watch(_meanConfig.gulpTask.templatesAndViews, ['refreshPageTrigger']);
});