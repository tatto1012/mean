# MEAN #

Integration between next technologies
* Mongo DB
* Express Js
* Angular Js
* Node Js

# Requirements #
* Node [more info](https://nodejs.org/en/download/)
* Mongo DB [more info](https://docs.mongodb.org/manual/tutorial/install-mongodb-on-os-x/)
* Gulp (Globally) [more info](https://www.npmjs.com/package/gulp)
* Bower (Globally) [more info](https://www.npmjs.com/package/bower)
* Sass (Globally) [more info](http://sass-lang.com/install)

# Getting Starter #

Run the next commands in your local directory for prepare the dependencies needed
```
#!console

npm install
bower install
```

Run the next commands in diferentes tabs of your local terminal for put to run the application
```
#!console

nodemon
gulp
```

# Directory Structure #
```
#!console
/mean (**root**)
../application (**access for all application files**)
..../controllers (**BE**)
..../models (**BE**)
..../routes (**BE**)
..../scripts (**FE**)
....../controller
....../directives
....../filters
....../services
......app.js
..../services (**BE**)
..../styles (**FE**)
....../mean-scss
..../views (**BE - FE**)
....../partials
......../some-folder-for-partials
........layout.ejs
....../templates (**FE**)
../assets (**Server static files **)
..../css
..../js
..../img
../bower_components
..../bower_dependecies
../node_modules
..../node_dependecies
../config (**BE**)
..../server_config_file.js
...projectfiles.any 
```