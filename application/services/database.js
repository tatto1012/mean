var _meanConfig = require('./../../config/mean.config');
var MongoDB = require('mongodb');
var _mongoCollection = null;

var mongoConectionInstance = function(collectionToUse, readyCallback) {

    if(!collectionToUse){
        readyCallback(_meanConfig.mongoDB.errorMessages.notCollectionSelected);
        return;
    }

    //Define the connection type
    var _mongoServerInstance = new MongoDB.Server(_meanConfig.mongoDB.host, _meanConfig.mongoDB.port, _meanConfig.mongoDB.options);
    //Define the access to database reference
    var _DB = new MongoDB.Db(_meanConfig.mongoDB.database, _mongoServerInstance);
    //Open Database Connection
    _DB.open(function(error, databaseConnection) {
        if(error){
            readyCallback(error);
        } else {

            readyCallback(null, databaseConnection.collection(collectionToUse));
        }
    });
};

module.exports = mongoConectionInstance;