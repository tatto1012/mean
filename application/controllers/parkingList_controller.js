var slotsModel = require('./../models/slots_model');

var ParkingList_Controller = function(){

    this.getListOfAllSlots = function(callback){
        slotsModel.getAllRecords(function(error, response){
            callback(error, response);
        });
    };

    this.getSlotByObjectID = function(objectID, callback){
        slotsModel.getRecordByObjectID(objectID, function(error, response){
            callback(error, response);
        });
    };
};

module.exports = new ParkingList_Controller();
