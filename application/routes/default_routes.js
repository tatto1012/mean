var express = require('express');
var async = require('async');
var router = express.Router();
var _meanConfig = require('./../../config/mean.config');

router.get('/', function(req, res, next){
    var ParkingListController = require('./../controllers/parkingList_controller');

    async.series([
        function(seriesCallback){
            ParkingListController.getListOfAllSlots(seriesCallback);
        }
    ], function(err, results){
        if(err){
            res.status(400);
            res.send(err);
        } else {
            res.render(_meanConfig.defaultLayout, {
                'viewPathReference' : './partials/home/index',
                'slotsData': results[0]
            });
        }
    });
});

router.get('/add-parking-slot', function(req, res, next){
    res.render(_meanConfig.defaultLayout, {
        'viewPathReference' : _meanConfig.views.addParkingSlot
    });
});

router.get('/edit-parking-slot/:slotIdentifier', function(req, res, next){
    var ParkingListController = require('./../controllers/parkingList_controller');
    var slotOID = req.params.slotIdentifier;

    async.series([
        function(seriesCallback){
            ParkingListController.getSlotByObjectID(slotOID, seriesCallback);
        }
    ], function(err, results){
        if(err){
            res.status(400);
            res.send(err);
        } else {
            res.render(_meanConfig.defaultLayout, {
                'viewPathReference' : _meanConfig.views.editParkingSlot,
                'slotData': results[0]
            });
        }
    });
});

module.exports = router;