var express = require('express');
var router = express.Router();
var _meanConfig = require('./../../config/mean.config');
var slotsModel = require('./../models/slots_model');

//Get default API request
router.get('/', function(req, res, next){
    res.send({'data':'Access to API'});
});

router.get('/get-all-parking-slot', function(req, res, next){
    slotsModel.getAllRecords(function(error, QueryResults){
        if(!error){
            var apiresponse = {
                'slotsData': QueryResults
            };
            res.send(apiresponse);
        } else {
            res.status(400);
            res.send(error);
        }
    });
});

router.get('/get-parking-slot-by-id/:slotIdentifier', function(req, res, next){
    var objectIdentifier = req.params.slotIdentifier;
    slotsModel.getRecordByObjectID(objectIdentifier, function(error, QueryResults){
        if(!error){
            var apiresponse = {
                'slotData': QueryResults
            };
            res.send(apiresponse);
        } else {
            res.status(400);
            res.send(error);
        }
    });
});

router.post('/add-parking-slot', function(req, res, next){
    var newSlotData = {
        'code' : req.body.parkingSlotCode,
        'campaign' : req.body.campaign,
        'status' : 'available'
    };

    slotsModel.addNewRecord(newSlotData, function(error, WriteResult){
        if(!error && WriteResult.result.ok){
            var apiresponse = {
                'message': 'Parking Slot added successfully',
                'idReferece': WriteResult.insertedIds[0]
            };
            res.send(apiresponse);
        } else {
            res.status(400);
            res.send(error);
        }
    });
});

router.post('/edit-parking-slot', function(req, res, next){

    var objectIdentifier = req.body.slotIdentifier;
    var updateSlotData = {
        'code': req.body.parkingSlotCode,
        'campaign': req.body.campaign,
        'status': req.body.slotStatus
    };


    slotsModel.updateRecordByObjectId(updateSlotData, objectIdentifier, function(error, UpdateResult){
        if(!error && UpdateResult.result.ok){
            var apiresponse = {
                'message': 'Parking Slot updated successfully'
            };
            res.send(apiresponse);
        } else {
            res.status(400);
            res.send(error);
        }
    });
});

router.post('/remove-parking-slot', function(req, res, next){

    var objectIdentifier = req.body.slotIdentifier;

    slotsModel.removeRecordByObjectId(objectIdentifier, function(error, RemoveResult){
        if(!error && RemoveResult.result.ok){
            var apiresponse = {
                'message': 'Parking Slot removed successfully'
            };
            res.send(apiresponse);
        } else {
            res.status(400);
            res.send(error);
        }
    });
});

module.exports = router;