var _ = require('lodash');
var _DB = require('./../services/database');
var ObjectId = require('mongodb').ObjectID;

var Slots_Model = function(){
    this.colectionReference = 'slots';

    this.getAllRecords = function(slotsModelCallback){
        _DB(this.colectionReference, function(error, slotCollection){
            slotCollection.find().toArray(function(err, queryResults) {
                slotsModelCallback(error, queryResults);
            });
        });
    };

    this.getRecordByObjectID = function(objectID, slotsModelCallback){
        var query = {
            "_id": ObjectId(objectID)
        };

        _DB(this.colectionReference, function(error, slotCollection){
            slotCollection.findOne(query, function(err, queryResults) {
                slotsModelCallback(error, queryResults);
            });
        });
    };

    this.addNewRecord = function(newSoltDocumentData, slotsModelCallback){
        _.extend(newSoltDocumentData, {"createdAt": new Date() });
        _DB(this.colectionReference, function(error, slotCollection){
            slotCollection.insert(newSoltDocumentData, slotsModelCallback);
        });
    };

    this.updateRecordByObjectId = function(updateSlotData, objectIdentifierString, slotsModelCallback){
        var updateObject = {
            $set: updateSlotData,
            $currentDate: { "lastModified": true }
        };

        var updateFilter = {
            '_id': ObjectId(objectIdentifierString)
        };

        _DB(this.colectionReference, function(error, slotCollection){
            slotCollection.updateOne(updateFilter, updateObject, function(err, updateResults) {
                slotsModelCallback(error, updateResults);
            });
        });
    };

    this.removeRecordByObjectId = function(objectIdentifierString, slotsModelCallback){
        var updateFilter = {
            '_id': ObjectId(objectIdentifierString)
        };

        _DB(this.colectionReference, function(error, slotCollection){
            slotCollection.deleteOne(updateFilter, slotsModelCallback);
        });
    };
};

module.exports = new Slots_Model();