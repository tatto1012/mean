app.factory('APIRequestService', [
    '_config',
    '_apiEndpointsList',
    '$http',
function (_config, _apiEndpointsList, $http) {
    var APIRequestService = this;

    APIRequestService.madeRequestAction = function(method, endpointReference, requestData, addTokenToRequest, callback){
        //define the http options
        var httpOptions = {
            method : method,
            url    : _config.APIroute + endpointReference,
            data   : requestData,
            cache  : false,
            timeout: _config.requestTimeout
        };

        $http(httpOptions).then(function successCallback(response) {
            callback(null, response);
        }, function errorCallback(response) {
            APIRequestService.handledRequestError(response, callback);
        });
    };

    APIRequestService.handledRequestError = function(errorResponse, callback){
        var errorMessage;

        switch(errorResponse.statusCode){
            case 400:
                    errorMessage = "Bad Request";
                break;
            case 401:
                    errorMessage = "Unauthorized";
                break;
            case 403:
                    errorMessage = "Forbidden";
                break;
            case 404:
                    errorMessage = "Not Found";
                break;
            case 408:
                    errorMessage = "Request Timeout";
                break;
            case 500:
                    errorMessage = "Internal Server Error";
                break;
            case 503:
                    errorMessage = "Service Unavailable";
                break;
            default:
                    errorMessage = 'Without Handled Error with code status ' + errorResponse.statusCode;
                break;
        }

        callback(errorMessage);
    };

    APIRequestService.addParkingSlot = function (requestData, callback){
        APIRequestService.madeRequestAction('POST',_apiEndpointsList.addParkingSlot, requestData,  false, callback);
    };

    APIRequestService.editParkingSlot = function (requestData, callback){
        APIRequestService.madeRequestAction('POST',_apiEndpointsList.editParkingSlot, requestData,  false, callback);
    };

    APIRequestService.removeParkingSlot = function (requestData, callback){
        APIRequestService.madeRequestAction('POST',_apiEndpointsList.removeParkingSlot, requestData,  false, callback);
    };

    return APIRequestService;
}]);