app.constant('appHelper', {

  templatesDir: '/templates',
  applicationDir: '/application/views',

  templatePath: function(view_name){
    return this.templatesDir + '/' + view_name + '.html';
  },

  applicationView: function(view_name){
    return this.applicationDir + '/' + view_name + '.html';
  }

});