app.controller('parkingSlotsListCtrl', [
    '$scope',
    '$rootScope',
function($scope, $rootScope){
    $scope.verifyRemoveAction = function(slotIdentifier, slotCode){
        $rootScope.showConfirmMessage("Are you sure of remove the <b>" + slotCode + "</b> slot?", slotIdentifier);
    };
}]);