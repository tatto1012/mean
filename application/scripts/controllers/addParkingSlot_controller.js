app.controller('addParkingSlotCtrl', [
    '$scope',
    '$rootScope',
    'APIRequestService',
function($scope, $rootScope, APIRequestService) {

    $scope.parkingSlotCode = '';
    $scope.campaign = 'Prodigious';

    $scope.addParkingSlotAction = function(formData){
        if(formData.$valid){
            var requestData = {
                'parkingSlotCode': $scope.parkingSlotCode,
                'campaign': $scope.campaign
            };

            APIRequestService.addParkingSlot(requestData, function(error, response){
                $rootScope.showFeedbackMessage(true, response);
            });
        } else {
            $rootScope.showFeedbackMessage(true, {
                'status': 400,
                'data' : {
                    'message' : "You have and error in your submission"
                }
            });
        }
    };

}]);