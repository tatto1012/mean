app.controller('editParkingSlotCtrl', [
    '$scope',
    '$rootScope',
    'APIRequestService',
function($scope, $rootScope, APIRequestService) {

    $scope.editParkingSlotAction = function(formData){
        if(formData.$valid){
            var requestData = {
                'parkingSlotCode': $scope.parkingSlotCode,
                'campaign': $scope.campaign,
                'slotStatus': $scope.slockStatus,
                'slotIdentifier': $scope.slotIdentifier
            };

            APIRequestService.editParkingSlot(requestData, function(error, response){
                $rootScope.showFeedbackMessage(true, response);
            });
        } else {
            $rootScope.showFeedbackMessage(true, {
                'status': 400,
                'data' : {
                    'message' : "You have and error in your submission"
                }
            });
        }
    };

}]);