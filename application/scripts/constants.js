app.constant('_config', {
    'appReference': 'MEAN',
    'appEnvironment': 'Development'
});

app.constant('_develompentVars', {
    'baseUrl': 'http://localhost:3000',
    'APIroute': 'http://localhost:3000/',
    'requestTimeout': 3000
});

app.constant('_productionVars', {
    'baseUrl': 'http://parking-manager',
    'APIroute': 'http://localhost:3000/',
    'requestTimeout': 60000
});

app.constant('_apiEndpointsList', {
    'getAllParkingSlot': 'api/get-all-parking-slot',
    'getParkingSlotById': 'api/get-parking-slot-by-id',
    'addParkingSlot': 'api/add-parking-slot',
    'editParkingSlot': 'api/edit-parking-slot',
    'removeParkingSlot': 'api/remove-parking-slot'
});

app.constant('_regExp', {
    'onlyNumbersPattern': "^[0-9]+$",
    'onlyAlphabethPattern': "^[a-z _-]+$"
});

app.constant('_feedbackMessageObject', {
    'message':'message not set yet!',
    'animation': false,
    'type': 'warning',
    'duration': 3000
});

app.constant('_confirmationMessage', {
    'message':'message of confirmation not set yet!',
    'animation': false,
    'type': 'warning',
    'duration': 3000,
    'itemIdentifier': false
});