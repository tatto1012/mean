app.directive( 'feedbackMessage', function(_config, appHelper){
    return {
        restrict: 'E',
        templateUrl: appHelper.templatePath('feedback-message')
    };
});