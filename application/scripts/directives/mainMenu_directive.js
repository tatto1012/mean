app.directive( 'mainMenu', function(_config, appHelper){
    return {
        restrict: 'E',
        templateUrl: appHelper.templatePath('main-menu'),
        link: function(scope, element, attrs){
            scope.menuItem = {
                listSlots : false,
                addParkingSlot : false
            };

            var currentPage = location.href.replace(_config.baseUrl, '');

            switch(currentPage){
                case '/':
                        scope.menuItem.listSlots = true;
                    break;
                case '/add-parking-slot':
                        scope.menuItem.addParkingSlot = true;
                    break;
            }
        }
    };
});