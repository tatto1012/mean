app.directive( 'statusIcon', function(appHelper){
    return {
        restrict: 'E',
        templateUrl: appHelper.templatePath('status-icon'),
        scope:{
            status: '=status',
        },
        link : function (scope, element, attrs){

            switch(attrs.status){
                case 'available':
                        scope.iconReference = 'circle-o';
                        scope.iconStyle = 'available';
                    break;
                case 'assigned':
                        scope.iconReference = 'check-circle-o';
                        scope.iconStyle = 'assigned';
                    break;
                case 'disabled':
                        scope.iconReference = 'circle';
                        scope.iconStyle = 'disabled';
                    break;
                default:
                        scope.iconReference = 'ellipsis-h';
                        scope.iconStyle = '';
                    break;
            }
        }
    };
});