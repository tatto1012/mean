app.directive( 'confirmationMessage', function(_config, appHelper){
    return {
        restrict: 'E',
        templateUrl: appHelper.templatePath('confirmation-message'),
        controller: [
            '$scope',
            '$rootScope',
            'APIRequestService',
        function($scope, $rootScope, APIRequestService){
            $scope.cancelAction = function(){
                $rootScope.confirmationMessage.animation = false;
            };

            $scope.confirmAction = function(){
                $rootScope.confirmationMessage.animation = false;
                var requestData = {
                    'slotIdentifier': $rootScope.confirmationMessage.itemIdentifier
                };

                APIRequestService.removeParkingSlot(requestData, function(error, response){
                    $rootScope.showFeedbackMessage(true, response);
                });
            };
        }]
    };
});