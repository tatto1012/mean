app.run([
    '_regExp',
    '_feedbackMessageObject',
    '_confirmationMessage',
    '$rootScope',
    '$timeout',
function(_regExp, _feedbackMessageObject, _confirmationMessage, $rootScope, $timeout) {
    _.extend($rootScope, _regExp);
    _.extend($rootScope, {'feedbackMessage':_feedbackMessageObject});
    _.extend($rootScope, {'confirmationMessage':_confirmationMessage});

    $rootScope.showFeedbackMessage = function(show, responseData){
        $rootScope.feedbackMessage.animation = show;
        $rootScope.feedbackMessage.message = responseData.data.message;
        $rootScope.feedbackMessage.type = (responseData.status === 200)? 'success' : 'danger';

        $timeout(function(){
            $rootScope.feedbackMessage.animation = false;
        }, $rootScope.feedbackMessage.duration);
    };

    $rootScope.showConfirmMessage = function(message, itemIdentifier){
        $rootScope.confirmationMessage.animation = true;
        $rootScope.confirmationMessage.message = message;
        $rootScope.confirmationMessage.type = 'warning';
        $rootScope.confirmationMessage.itemIdentifier = itemIdentifier;
    };

}]);