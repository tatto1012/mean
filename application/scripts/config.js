app.config(function(_config, _develompentVars, _productionVars, $provide){
    switch(_config.appEnvironment){
        case 'Production':
            $provide.constant('_config', _.extend(_config, _productionVars) );
            break;
        case 'Development':
            $provide.constant('_config', _.extend(_config, _develompentVars) );
            break;
    }
});