var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var lodash = require('lodash');

//Get the configuration of the application
var _meanConfig = require('./config/mean.config');

//Add application routes
var routes = require(_meanConfig.server.routes.default);
var apiEndpoints = require(_meanConfig.server.routes.apiEndpoints);

//initialize express application
var app = express();

//Made accessible lodash features in all application and define constants
app.locals._ = lodash;
app.locals.baseUrl = 'http://localhost:3000/';

//Define the view engine application
app.set('views', path.join(__dirname, _meanConfig.server.views.path));
app.set('view engine', _meanConfig.server.views.engine);

//Define the parser for params and cookies

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

//Define the public directories routes
app.use('/assets', express.static(path.join(__dirname, _meanConfig.server.staticFiles)));
app.use('/templates', express.static(__dirname + _meanConfig.server.templatesFiles));
//User action for each access route
app.use('/', routes); //application routes
app.use('/api', apiEndpoints); //api routes

//Catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = _meanConfig.server.fileRouteNotFound.data.message;
  err.status = _meanConfig.server.fileRouteNotFound.code;
  next(err);
});

//Launch listener in port for all request handler
app.listen(_meanConfig.server.port, function () {
  console.log('App listening on port '+ _meanConfig.server.port +'!');
});

