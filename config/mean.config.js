var _meanConfiguration = {
    "server": {
        "port" : 3000,                                                       //Port to listen the access to application
        "fileRouteNotFound": {                                               //File not found message
            "code": 404,                                                     //Code of the output message
            "path": '',                                                      //Path of the file requested
            "data": {                                                        //Data or info to output into template
                "message": "Not Found"                                       //output message
            }
        },
        "routes" : {                                                         //List of all routes of the application
            "default": "./application/routes/default_routes",
            "apiEndpoints": "./application/routes/apiEndpoints_routes"
        },
        "views" : {
            "path": "application/views",                                     //default views path
            "engine": "ejs"                                                  //default type of markup engine
        },
        "staticFiles": "assets",                                              //Public access files JS,CSS,images,etc..
        "templatesFiles": "/application/views/templates"                       //Public access for templates for directives
    },
    "mongoDB":{
        "host": "localhost",                                                 //Domain of access to mongo database connection
        "port": "27017",                                                     //Port of access to mongo database connection
        "database": "MEAN-DB",                                               //Database Name reference to access
        "options": {                                                         //MongoDB connection options
            "auto_reconnect": true
        },
        "errorMessages": {                                                   //output messages for status of connection
            "notDatabaseSelected": "Please define the database to use",
            "notCollectionSelected": "Plase define the collection to use"
        }
    },
    "gulpTask": {
        "buildDirectories": {                                                //Define the files routes to generate and concatenate
            "js": "./assets/js/",
            "css": "./assets/css/"
        },
        "targetNames": {                                                     //Define the file name reference to generate
            "appJs": "app.js",
            "vendorJs": "vendor.js",
            "appCss": "styles.css",
            "vendorCss": "vendor.css"
        },
        "sassPaths" : {                                                      //Define the files initialize with the sass compiler
            'appStyles': './application/styles/appStyles.scss',
            'vendor': './application/styles/vendor.scss'
        },
        "appStyles":[                                                        //Define the application scss files to watch for changes
            "./application/styles/*.scss",
            "./application/styles/**/*.scss",
            "./application/styles/**/**/*.scss",
        ],
        "vendorStyles":[                                                     //Define the vendor scss files to watch for changes
            "./bower_components/bootstrap-sass/assets/stylesheets/_bootstrap.scss"
        ],
        "appScripts": [                                                      //Define the scripts to concatenate of the application
            "application/scripts/app.js",
            "application/scripts/run.js",
            "application/scripts/config.js",
            "application/scripts/constants.js",
            "application/scripts/controllers/*",
            "application/scripts/controllers/**/*",
            "application/scripts/directives/*",
            "application/scripts/directives/**/*",
            "application/scripts/filters/*",
            "application/scripts/filters/**/*",
            "application/scripts/services/*",
            "application/scripts/services/**/*"
        ],
        "vendorScripts": [                                                   //Define the vendor scripts to concatenate or to use
            "bower_components/angular/angular.min.js",
            "bower_components/jquery/dist/jquery.min.js",
            "node_modules/lodash/lodash.min.js",
            "bower_components/bootstrap-sass/assets/javascripts/bootstrap.min.js",
            "bower_components/moment/min/moment.min.js",
            "bower_components/angular-fontawesome/dist/angular-fontawesome.min.js"
        ],
        "templatesAndViews": [                                              //Define the watch for changes in the markup of the views application
            "application/views/*",
            "application/views/**/*",
            "application/views/**/**/*"
        ],
        "cleanFilesList": [                                                  //Define the files to clean for each compile and refresh when changes occur
            'assets/js/app.js',
            'assets/js/vendor.js'
        ],
        "openBrowser": {                                                     //Define the browser configuration to open when run gulp
            "url": "http://localhost:3000",
            "browser": "Google Chrome" // [firefox, chrome, google-chrome]
        }
    },
    "defaultLayout": "layout",                                               //Define the reference for default layout.
    "views" : {
        "addParkingSlot" : "./partials/parking/add-parking-slot",
        "editParkingSlot" : "./partials/parking/edit-parking-slot"
    }
};

module.exports = _meanConfiguration;